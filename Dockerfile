FROM ubuntu:18.04

RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y  wget openjdk-8-jdk maven vim && \
    apt-get clean

RUN mkdir -p /tmp/timex
WORKDIR /tmp/timex

RUN wget http://archive.apache.org/dist/uima/uimaj-2.6.0/uimaj-2.6.0-bin.tar.gz
RUN tar xvfz uimaj-2.6.0-bin.tar.gz

#set UIMA_HOME to the path of your "apache-uima" folder

ENV UIMA_HOME /tmp/timex/apache-uima

#make sure that JAVA_HOME is set correctly
#        - add the "$UIMA_HOME/bin" to your PATH
#    * Adjust the UIMA's example paths:
#    * For further information about UIMA, see http://uima.apache.org/
ENV UIMA_HOME /tmp/timex/apache-uima
ENV PATH $PATH:$UIMA_HOME/bin
RUN $UIMA_HOME/bin/adjustExamplePaths.sh

#2. Download and install the UIMA HeidelTime kit
#    * download the latest heideltime-kit from
#      https://github.com/HeidelTime/heideltime/releases
#    * unzip or untar the heideltime-kit into a path called HEIDELTIME_HOME from hereon out.
#    * set the environment variable HEIDELTIME_HOME (you can set these variables globally, 
#      e.g., in your $HOME/.bashrc):
#        - export HEIDELTIME_HOME='/path/to/heideltime/'

RUN wget https://github.com/HeidelTime/heideltime/releases/download/VERSION2.2.1/heideltime-kit-2.2.1.tar.gz && \
	tar xvfz heideltime-kit-2.2.1.tar.gz
ENV HEIDELTIME_HOME /tmp/timex/heideltime-kit
	

#3. HeidelTime requires sentence, token, and part-of speech annotations. We have developed
#   our own wrapper for the popular TreeTagger tool that will support any language for which
#   there are parameter files available.
#   However, you may use other tools (if so, you either have to adjust the analysis engine
#   "Annotation Translator", which is part of the UIMA HeidelTime kit or you may adjust the 
#   imports in HeidelTime.java itself. Furthermore, if a differing tag set is used, all rules
#   containing part-of-speech information have to be adapted).
#   
#   To process English, German, Dutch, Spanish, Italian, French, Chinese or Russian documents, 
#   the TreeTaggerWrapper can be used for pre-processing: 
#    * Download the TreeTagger and its tagging scripts, installation scripts, as well as 
#      English, German, and Dutch (and all required) parameter files into one directory from:
#      http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/
#      - mkdir treetagger 
#      - cd treetagger
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2.1.tar.gz
#      - or alternatively: wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2-old.tar.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagger-scripts.tar.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/install-tagger.sh
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german-par-linux-3.2-utf8.bin.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english-par-linux-3.2-utf8.bin.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/dutch-par-linux-3.2-utf8.bin.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian-par-linux-3.2-utf8.bin.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish-par-linux-3.2-utf8.bin.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french-par-linux-3.2-utf8.bin.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/portuguese-par-linux-3.2-utf8.bin.gz
#      - wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/estonian-par-linux-3.2-utf8.bin.gz
#      Attention: If you do not use Linux, please download all TreeTagger files directly from
#                 http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/

RUN cd heideltime-kit && mkdir treetagger && \ 
	cd treetagger && \
	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2.3.tar.gz && \
	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagger-scripts.tar.gz && \
	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/install-tagger.sh && \
	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german.par.gz && \
#	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/dutch.par.gz && \
#	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian.par.gz && \
	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish.par.gz && \
	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french.par.gz && \
#	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/portuguese.par.gz && \
	wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english.par.gz

#    * Install the TreeTagger
#        - sh install-tagger.sh
RUN cd heideltime-kit/treetagger && sh install-tagger.sh

#    * Set environment variables (you can set variables permanently, e.g., in your $HOME/.bashrc)
#      and then source the environment.
#        - export TREETAGGER_HOME='path to TreeTagger'
#        - source $HEIDELTIME_HOME/metadata/setenv
ENV TREETAGGER_HOME /tmp/timex/heideltime-kit/treetagger
#RUN source $HEIDELTIME_HOME/metadata/setenv
RUN /bin/bash -c "source $HEIDELTIME_HOME/metadata/setenv"

RUN mkdir /var/maven/ && chmod -R 777 /var/maven && mkdir /tmp/timex/etimex/ && chmod -R 777 /tmp/timex/etimex && mkdir /tmp/timex/etimex/lib/ && chmod -R 777 /tmp/timex/etimex/lib
ENV MAVEN_CONFIG /var/maven/.m2

ADD pom.xml /tmp/timex/etimex
ADD lib/* /tmp/timex/etimex/lib/

RUN cd /tmp/timex/etimex && mvn -B -e -C -T 1C -Duser.home=/var/maven org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline 

COPY . /tmp/timex/etimex/

ADD src/main/resources/config/config.props /tmp/timex/heideltime-kit/conf

RUN chmod -R 777 /var/maven
EXPOSE 8034

WORKDIR /tmp/timex/etimex
RUN mvn -Duser.home=/var/maven clean install -DskipTests

ADD src/main/resources/application.properties /tmp/timex/etimex/target/classes

RUN chmod -R 777 /tmp/timex/etimex

RUN apt-get install -y  apt-utils locales && \
    apt-get clean

RUN locale-gen de_DE.UTF-8

ENV LANG de_DE.UTF-8  
ENV LANGUAGE de_DE:de  
ENV LC_ALL de_DE.UTF-8

#CMD mvn -Duser.home=/var/maven -Drun.jvmArguments="-Xmx6144m" spring-boot:run
CMD mvn -Duser.home=/var/maven spring-boot:run
#CMD ["/bin/bash"]


