package de.dfki.eservices;


import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

import de.dfki.ETimexApplication;

/**
 * @author 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ETimexApplication.class })
@WebAppConfiguration
public class ETimexTest {

	
	String port = "8095";
	
	@Before
	public void setup() {
//		ApplicationContext context = IntegrationTestSetup.getContext(TestConstants.pathToPackage);
//		context = IntegrationTestSetup.getContext(TestConstants.pathToPackage);
//		testHelper = context.getBean(TestHelper.class);
//		validationHelper = context.getBean(ValidationHelper.class); 
	}
	
	private HttpRequestWithBody baseTimexRequest() {
		String url = "http://localhost:" + port + "" + "/eTimex/testURL";
		return Unirest.post(url);
	}
	
	private HttpRequestWithBody eTimexPostRequest(String path) {
		String url = "http://localhost:" + port + "" + "/eTimex/"+path;
		return Unirest.post(url);
	}
	
	private GetRequest eTimexGetRequest(String path) {
		String url = "http://localhost:" + port + "" + "/eTimex/"+path;
		return Unirest.get(url);
	}
	
	@Test
	public void sanityCheckETimex() throws UnirestException, IOException,
			Exception {
		// sanity check
		HttpResponse<String> response = baseTimexRequest()
				.queryString("informat", "text")
				.queryString("input", "hello world")
				.queryString("outformat", "turtle").asString();

		Assert.assertTrue(response.getStatus() == 200);
		Assert.assertTrue(response.getBody().length() > 0);
	}
	
//	@Test
//	public void turtleInTurtleOutTempAnalysisEn() throws UnirestException, IOException,
//			Exception {
//
//				HttpResponse<String> response371 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "en")
//				.queryString("models", "englishDates")
//				.queryString("informat", "turtle")
//				.queryString("outformat", "turtle")
//				.body(TestConstants.expectedResponse5)
//				.asString();
//		
//				Assert.assertTrue(response371.getStatus() == 200);
//		Assert.assertTrue(response371.getBody().length() > 0);
//		
////		Assert.assertEquals(TestConstants.expectedResponse16, response16.getBody());
//		
//		Model mExp = NIFReader.extractModelFromFormatString(TestConstants.expectedResponse234, RDFSerialization.TURTLE);
//		Model mAct = NIFReader.extractModelFromFormatString(response371.getBody(), RDFSerialization.TURTLE);
//		
////		assertTrue(mExp.isIsomorphicWith(mAct));
//		
//		String es = null;
//		String ee = null;
//		String as = null;
//		String ae = null;
//		
//		NodeIterator nIt1 = mExp.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mExp), DKTNIF.meanDateStart);
//		while(nIt1.hasNext()){
//			RDFNode n1 = nIt1.next();
//			es = n1.asLiteral().getString();
//		}
//		NodeIterator nIt12 = mExp.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mExp), DKTNIF.meanDateEnd);
//		while(nIt12.hasNext()){
//			RDFNode n1 = nIt12.next();
//			ee = n1.asLiteral().getString();
//		}
//
//		NodeIterator nIt2 = mAct.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mAct), DKTNIF.meanDateStart);
//		while(nIt2.hasNext()){
//			RDFNode n2 = nIt2.next();
//			as = n2.asLiteral().getString();
//		}
//		NodeIterator nIt22 = mAct.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mAct), DKTNIF.meanDateEnd);
//		while(nIt22.hasNext()){
//			RDFNode n2 = nIt22.next();
//			ae = n2.asLiteral().getString();
//		}
//
//		Date d11 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(es);
//		Date d12 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ee);
//		Date d21 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(as);
//		Date d22 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ae);
//		
//		System.out.println("Initial_11_" + (d11.getTime() - d21.getTime()) );
//		System.out.println("final_11_" + (d11.getTime() - d21.getTime()) );
//		Assert.assertTrue( Math.abs(d11.getTime() - d21.getTime()) < 60000 );
//		Assert.assertTrue( Math.abs(d12.getTime() - d22.getTime()) < 60000 );
//
//		
//	}
//	
//	@Test
//	public void seasonBugfixTest() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response9 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "de")
//				.queryString("models", "germanDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				.body("2016 blabla Sommer")
//				.asString();
//		
//		Assert.assertEquals(TestConstants.expectedResponse9, response9.getBody());
//		Assert.assertTrue(response9.getStatus() == 200);
//		Assert.assertTrue(response9.getBody().length() > 0);
//		
//	}
//	
//	@Test
//	public void addedHolidaysTest() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response10 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "de")
//				.queryString("models", "germanDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				.body("2014 silvester neujahr tag der arbeit maifeiertag tag der deutschen einheit")
//				.asString();
//		
//		Assert.assertEquals(TestConstants.expectedResponse10, response10.getBody());
//		Assert.assertTrue(response10.getStatus() == 200);
//		Assert.assertTrue(response10.getBody().length() > 0);
//		
//	}
//	
//	@Test
//	public void addedTimesAndMoreDatesTest() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response11 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "de")
//				.queryString("models", "germanDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				.body("Tele 5 zeigt am 17.12. um 20.15 Uhr und um 15 Uhr die Komödie")
//				.asString();
//		
//		Assert.assertEquals(TestConstants.expectedResponse11, response11.getBody());
//		Assert.assertTrue(response11.getStatus() == 200);
//		Assert.assertTrue(response11.getBody().length() > 0);
//		
//	}
//	
//	@Test
//	public void addedThisWeekYearTest() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response12 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "de")
//				.queryString("models", "germanDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				.body("08.10.1990 dieser Tag diese Woche dieser Monat dieses Jahr")
//				//.body("08.10.2016 dieser Tag diese Woche dieser Monat dieses Jahr")
//				.asString();
//		Assert.assertTrue(response12.getStatus() == 200);
//		Assert.assertTrue(response12.getBody().length() > 0);
//		
////		Assert.assertEquals(TestConstants.expectedResponse16, response16.getBody());
//		
//		Model mExp = NIFReader.extractModelFromFormatString(TestConstants.expectedResponse12, RDFSerialization.TURTLE);
//		Model mAct = NIFReader.extractModelFromFormatString(response12.getBody(), RDFSerialization.TURTLE);
//		
////		assertTrue(mExp.isIsomorphicWith(mAct));
//		
//		String es = null;
//		String ee = null;
//		String as = null;
//		String ae = null;
//		
//		NodeIterator nIt1 = mExp.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mExp), DKTNIF.meanDateStart);
//		while(nIt1.hasNext()){
//			RDFNode n1 = nIt1.next();
//			es = n1.asLiteral().getString();
//		}
//		NodeIterator nIt12 = mExp.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mExp), DKTNIF.meanDateEnd);
//		while(nIt12.hasNext()){
//			RDFNode n1 = nIt12.next();
//			ee = n1.asLiteral().getString();
//		}
//
//		NodeIterator nIt2 = mAct.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mAct), DKTNIF.meanDateStart);
//		while(nIt2.hasNext()){
//			RDFNode n2 = nIt2.next();
//			as = n2.asLiteral().getString();
//		}
//		NodeIterator nIt22 = mAct.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mAct), DKTNIF.meanDateEnd);
//		while(nIt22.hasNext()){
//			RDFNode n2 = nIt22.next();
//			ae = n2.asLiteral().getString();
//		}
//
//		Date d11 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(es);
//		Date d12 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ee);
//		Date d21 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(as);
//		Date d22 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ae);
//		
//		System.out.println("Initial_22_" + (d11.getTime() - d21.getTime()) );
//		System.out.println("final_22_" + (d11.getTime() - d21.getTime()) );
//		Assert.assertTrue( Math.abs(d11.getTime() - d21.getTime()) < 60000 );
//		Assert.assertTrue( Math.abs(d12.getTime() - d22.getTime()) < 60000 );
//
//	}
//	
//	@Test
//	public void addedSinceAndAgo() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response13 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "de")
//				.queryString("models", "germanDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				.body("1990 seit 10 Jahren, 2010 vor 10 Jahren")
//				.asString();
//		
//		Assert.assertEquals(TestConstants.expectedResponse13, response13.getBody());
//		Assert.assertTrue(response13.getStatus() == 200);
//		Assert.assertTrue(response13.getBody().length() > 0);
//		
//	}
//	
//	@Test
//	public void addedEnglishWeekdaysTest() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response14 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "en")
//				.queryString("models", "englishDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				.body("8.10.1990 monday tuesday friday")
//				.asString();
//		
//		Assert.assertEquals(TestConstants.expectedResponse14, response14.getBody());
//		Assert.assertTrue(response14.getStatus() == 200);
//		Assert.assertTrue(response14.getBody().length() > 0);
//		
//	}
//	
//	@Test
//	public void addedEnglishTimeTest() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response15 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "en")
//				.queryString("models", "englishDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				.body("8.10.1990 10.34 p.m. 9.00 a.m.")
//				.asString();
//		
//		Assert.assertEquals(TestConstants.expectedResponse15, response15.getBody());
//		Assert.assertTrue(response15.getStatus() == 200);
//		Assert.assertTrue(response15.getBody().length() > 0);
//		
//	}
//	
//	@Test
//	public void addedNextDayWeekEctTest() throws UnirestException, IOException,
//			Exception {
//
//		// plain text as input, turtle as output
//		HttpResponse<String> response16 = analyzeOpennlpRequest()
//				.queryString("analysis", "temp")
//				.queryString("language", "de")
//				.queryString("models", "germanDates")
//				//.queryString("informat", "turtle")
//				//.queryString("outformat", "turtle")
//				//.body("8.10.1990 nächster Tag nächste Woche nächster Monat nächstes Jahr")
//				.body("8.10.2015 nächster Tag nächste Woche nächster Monat nächstes Jahr")
//				.asString();
//		Assert.assertTrue(response16.getStatus() == 200);
//		Assert.assertTrue(response16.getBody().length() > 0);
//		
////		Assert.assertEquals(TestConstants.expectedResponse16, response16.getBody());
//		
//		Model mExp = NIFReader.extractModelFromFormatString(TestConstants.expectedResponse16, RDFSerialization.TURTLE);
//		Model mAct = NIFReader.extractModelFromFormatString(response16.getBody(), RDFSerialization.TURTLE);
//		
//	//	assertTrue(mExp.isIsomorphicWith(mAct));
//		
//		String es = null;
//		String ee = null;
//		String as = null;
//		String ae = null;
//		
//		NodeIterator nIt1 = mExp.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mExp), DKTNIF.meanDateStart);
//		while(nIt1.hasNext()){
//			RDFNode n1 = nIt1.next();
//			es = n1.asLiteral().getString();
//		}
//		NodeIterator nIt12 = mExp.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mExp), DKTNIF.meanDateEnd);
//		while(nIt12.hasNext()){
//			RDFNode n1 = nIt12.next();
//			ee = n1.asLiteral().getString();
//		}
//
//		NodeIterator nIt2 = mAct.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mAct), DKTNIF.meanDateStart);
//		while(nIt2.hasNext()){
//			RDFNode n2 = nIt2.next();
//			as = n2.asLiteral().getString();
//		}
//		NodeIterator nIt22 = mAct.listObjectsOfProperty(NIFReader.extractDocumentResourceURI(mAct), DKTNIF.meanDateEnd);
//		while(nIt22.hasNext()){
//			RDFNode n2 = nIt22.next();
//			ae = n2.asLiteral().getString();
//		}
//
//		Date d11 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(es);
//		Date d12 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ee);
//		Date d21 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(as);
//		Date d22 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ae);
//		
//		System.out.println(response16.getBody());
//		
//		System.out.println("Initial_33_" + (d11.getTime() - d21.getTime()) );
//		System.out.println("final_33_" + (d12.getTime() - d22.getTime()) );
//		Assert.assertTrue( Math.abs(d11.getTime() - d21.getTime()) < 60000 );
//		Assert.assertTrue( Math.abs(d12.getTime() - d22.getTime()) < 60000 );
//	}
//	
	
}
