package de.dfki.eservices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.dfki.eservices.etimex.ETimexService;


@RestController
@RequestMapping("/srv-timex")
public class ETimexRestController {

	Logger logger = Logger.getLogger(ETimexRestController.class);
	
	@Autowired
	ETimexService timexService;
	
	@RequestMapping(value = "/sanityCheck", method = { RequestMethod.POST, RequestMethod.GET })
	public ResponseEntity<String> testURL(
            @RequestBody(required = false) String postBody) throws Exception {
    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.add("Content-Type", "text/plain");
    	ResponseEntity<String> response = new ResponseEntity<String>("The srv-timex restcontroller is working properly", responseHeaders, HttpStatus.OK);
    	return response;
	}

	@RequestMapping(value = "/analyzeText", method = {RequestMethod.POST})
	public ResponseEntity<String> analyzeText(
			HttpServletRequest request,
//			@RequestParam(value = "analysis", required = false) String analysis, // either ner or dict or temp or tfidf
//			@RequestParam(value = "models", required = false) String models,
			@RequestParam(value = "language", required = false) String language,
			@RequestParam(value = "content", required = true) boolean isContent,
			@RequestParam(value = "outputCallback", required = false) String outputCallback,
			@RequestParam(value = "statusCallback", required = false) String statusCallback,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		
//		if (analysis == null || analysis.equalsIgnoreCase("")) {
//			logger.error("Error: 'analysis' input parameter can not be null or empty. The folowing values are supported: 'opennlp' and 'heideltime'.");
//			throw new Exception("Error: 'analysis' input parameter can not be null or empty. The folowing values are supported: 'opennlp' and 'heideltime'.");
//		}
//		if( !(analysis.equalsIgnoreCase("heideltime"))) {
//			logger.error("Error: 'analysis' values is not supported. Only the following values are supported: 'heideltime'.");
//			throw new Exception("Error: 'analysis' values is not supported. Only the following values are supported: 'heideltime'.");
//		}
		if (language == null || language.equalsIgnoreCase("")) {
			language = "en";
			logger.warn("The input language is not defined. English (\"EN\") is used.");
		}
//		if (models== null || models.equalsIgnoreCase("")) {
//			logger.warn("Models input parameter is null or empty. Models= is used");
//			//throw new Exception("'models' input parameter can not be null or empty.");
//			models = "all";
//		}
		if (postBody == null || postBody.equalsIgnoreCase("")) {
			logger.error("Error: body can not be null or empty. It must contain data for processing or document URL.");
			throw new Exception("Error: body can not be null or empty. It must contain data for processing or document URL.");
		}
		if (contentTypeHeader == null || contentTypeHeader.equalsIgnoreCase("")) {
			contentTypeHeader = "text/plain";
			logger.warn("The Content-Type is not defined. 'text/plain' is used.");
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "text/turtle";
			logger.warn("The Accept Header is not defined. 'text/turtle' is used.");
		}
       	try {
            String textForProcessing = postBody;
            HttpHeaders responseHeaders = new HttpHeaders();
			if(outputCallback==null || outputCallback.equalsIgnoreCase("")) {
//	    		String result = timexService.analyzeSynchronous(textForProcessing, contentTypeHeader, acceptHeader, analysis, models, null, language, isContent, outputCallback);
	    		String result = timexService.analyzeSynchronous(textForProcessing, contentTypeHeader, acceptHeader, language, isContent, outputCallback);
	    		if(isContent) {
	                responseHeaders.add("Content-Type", acceptHeader);
	    		}
	    		else {
	    			//TODO Store the resulting information in the triple store.
//	    			NIFAdquirer.saveNIFDocumentInLKGManager(result, acceptHeader);
	    			result = "ERROR: getting NIF from external sources (such as triple store) is still not supported!!";
	                responseHeaders.add("Content-Type", "text/plain");
	    		}
	    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
	            return response;
			}
			else {
	    		String result = timexService.analyzeAsynchronous(textForProcessing, contentTypeHeader, acceptHeader, language, isContent, outputCallback);
//	    		String result = service.analyzeAsynchronous(textForProcessing, contentTypeHeader, acceptHeader, prefix, language, analysis, models, isContent, outputCallback);
                responseHeaders.add("Content-Type", acceptHeader);
	    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.ACCEPTED);
	            return response;
			}
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/", method = {RequestMethod.POST})
	public ResponseEntity<String> analyzeTextELG(
			HttpServletRequest request,
			@RequestParam(value = "language", required = false) String language,
			@RequestParam(value = "creationDate", required = false) String date,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		if (language == null || language.equalsIgnoreCase("")) {
			language = "en";
			logger.warn("The input language is not defined. English (\"EN\") is used.");
		}
		if (postBody == null || postBody.equalsIgnoreCase("")) {
			logger.error("Error: body can not be null or empty. It must contain data for processing.");
			throw new Exception("Error: body can not be null or empty. It must contain data for processing.");
		}
		if (contentTypeHeader == null || contentTypeHeader.equalsIgnoreCase("")) {
			contentTypeHeader = "text/plain";
			logger.warn("The Content-Type is not defined. 'text/plain' is used.");
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "text/turtle";
			logger.warn("The Accept Header is not defined. 'text/turtle' is used.");
		}
       	try {
            String textForProcessing = postBody;
            HttpHeaders responseHeaders = new HttpHeaders();
            Date creationDate = null;
            if(date==null) {
            	creationDate = new Date();
            }
            else {
            	creationDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);  
            }
	    	String result = timexService.analyzeELG(textForProcessing, contentTypeHeader, acceptHeader, language, creationDate);
	    	responseHeaders.add("Content-Type", "application/json");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/en", method = {RequestMethod.POST})
	public ResponseEntity<String> analyzeTextELG_EN(
			HttpServletRequest request,
			@RequestParam(value = "creationDate", required = false) String date,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		String language = "en";
		if (postBody == null || postBody.equalsIgnoreCase("")) {
			logger.error("Error: body can not be null or empty. It must contain data for processing or document URL.");
			throw new Exception("Error: body can not be null or empty. It must contain data for processing or document URL.");
		}
		if (contentTypeHeader == null || contentTypeHeader.equalsIgnoreCase("")) {
			contentTypeHeader = "text/plain";
			logger.warn("The Content-Type is not defined. 'text/plain' is used.");
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "text/turtle";
			logger.warn("The Accept Header is not defined. 'text/turtle' is used.");
		}
       	try {
            String textForProcessing = postBody;
            HttpHeaders responseHeaders = new HttpHeaders();
            Date creationDate = null;
            if(date==null) {
            	creationDate = new Date();
            }
            else {
            	creationDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);  
            }
	    	String result = timexService.analyzeELG(textForProcessing, contentTypeHeader, acceptHeader, language, creationDate);
	    	responseHeaders.add("Content-Type", "application/json");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/de", method = {RequestMethod.POST})
	public ResponseEntity<String> analyzeTextELG_DE(
			HttpServletRequest request,
			@RequestParam(value = "creationDate", required = false) String date,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		String language = "de";
		if (postBody == null || postBody.equalsIgnoreCase("")) {
			logger.error("Error: body can not be null or empty. It must contain data for processing or document URL.");
			throw new Exception("Error: body can not be null or empty. It must contain data for processing or document URL.");
		}
		if (contentTypeHeader == null || contentTypeHeader.equalsIgnoreCase("")) {
			contentTypeHeader = "text/plain";
			logger.warn("The Content-Type is not defined. 'text/plain' is used.");
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "text/turtle";
			logger.warn("The Accept Header is not defined. 'text/turtle' is used.");
		}
       	try {
            String textForProcessing = postBody;
            HttpHeaders responseHeaders = new HttpHeaders();
            Date creationDate = null;
            if(date==null) {
            	creationDate = new Date();
            }
            else {
            	creationDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);  
            }
	    	String result = timexService.analyzeELG(textForProcessing, contentTypeHeader, acceptHeader, language, creationDate);
	    	responseHeaders.add("Content-Type", "application/json");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/es", method = {RequestMethod.POST})
	public ResponseEntity<String> analyzeTextELG_ES(
			HttpServletRequest request,
			@RequestParam(value = "creationDate", required = false) String date,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		String language = "es";
		if (postBody == null || postBody.equalsIgnoreCase("")) {
			logger.error("Error: body can not be null or empty. It must contain data for processing or document URL.");
			throw new Exception("Error: body can not be null or empty. It must contain data for processing or document URL.");
		}
		if (contentTypeHeader == null || contentTypeHeader.equalsIgnoreCase("")) {
			contentTypeHeader = "text/plain";
			logger.warn("The Content-Type is not defined. 'text/plain' is used.");
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "text/turtle";
			logger.warn("The Accept Header is not defined. 'text/turtle' is used.");
		}
       	try {
            String textForProcessing = postBody;
            HttpHeaders responseHeaders = new HttpHeaders();
            Date creationDate = null;
            if(date==null) {
            	creationDate = new Date();
            }
            else {
            	creationDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);  
            }
	    	String result = timexService.analyzeELG(textForProcessing, contentTypeHeader, acceptHeader, language, creationDate);
	    	responseHeaders.add("Content-Type", "application/json");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/fr", method = {RequestMethod.POST})
	public ResponseEntity<String> analyzeTextELG_FR(
			HttpServletRequest request,
			@RequestParam(value = "creationDate", required = false) String date,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		String language = "fr";
		if (postBody == null || postBody.equalsIgnoreCase("")) {
			logger.error("Error: body can not be null or empty. It must contain data for processing or document URL.");
			throw new Exception("Error: body can not be null or empty. It must contain data for processing or document URL.");
		}
		if (contentTypeHeader == null || contentTypeHeader.equalsIgnoreCase("")) {
			contentTypeHeader = "text/plain";
			logger.warn("The Content-Type is not defined. 'text/plain' is used.");
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "text/turtle";
			logger.warn("The Accept Header is not defined. 'text/turtle' is used.");
		}
       	try {
            String textForProcessing = postBody;
            HttpHeaders responseHeaders = new HttpHeaders();
            Date creationDate = null;
            if(date==null) {
            	creationDate = new Date();
            }
            else {
            	creationDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);  
            }
	    	String result = timexService.analyzeELG(textForProcessing, contentTypeHeader, acceptHeader, language, creationDate);
	    	responseHeaders.add("Content-Type", "application/json");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/checkStatus", method = {RequestMethod.GET})
	public ResponseEntity<String> checkStatus(
			HttpServletRequest request,
			@RequestParam(value = "analysisId", required = false) String analysisId,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		
		if (analysisId == null || analysisId.equalsIgnoreCase("")) {
			logger.error("Error: 'analysisId' input parameter can not be null or empty. The folowing values are supported: 'language', 'dictionary' and 'rules'.");
			throw new Exception("Error: 'analysisId' input parameter can not be null or empty. The folowing values are supported: 'language', 'dictionary' and 'rules'.");
		}
       	try {
        	String result = timexService.checkStatus(analysisId);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Content-Type", "text/plain");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/getOutput", method = {RequestMethod.GET})
	public ResponseEntity<String> getOutput(
			HttpServletRequest request,
			@RequestParam(value = "analysisId", required = false) String analysisId,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		
		if (analysisId == null || analysisId.equalsIgnoreCase("")) {
			logger.error("Error: 'analysisId' input parameter can not be null or empty.");
			throw new Exception("Error: 'analysisId' input parameter can not be null or empty.");
		}
       	try {
        	String result = timexService.getOutput(analysisId,contentTypeHeader);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Content-Type", "text/plain");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/eTimex/listModels", method = {
            RequestMethod.POST, RequestMethod.GET })
	public ResponseEntity<String> listModels(
			HttpServletRequest request,
			@RequestParam(value = "analysis", required = false) String analysis, // either ner or dict
			@RequestParam(value = "prefix", required = false) String prefix,
			@RequestParam(value = "p", required = false) String p,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestParam Map<String, String> allParams,
            @RequestBody(required = false) String postBody) throws Exception {
        
		// Check the document or directory parameter.
        if(analysis == null) {
			String msg = "Unspecified analysis type.";
			logger.error(msg);
			throw new Exception(msg);
        }
        
        String models = "";
        models += "germanDates" + "\n";
        models += "englishDates" + "\n";
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/plain");
        ResponseEntity<String> response = new ResponseEntity<String>(models, responseHeaders, HttpStatus.OK);
        return response;
	}

}

	
