package de.dfki.eservices.etimex.eheideltime;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import de.qurator.commons.Label;
import de.qurator.commons.LabelPositionAnnotation;
import de.qurator.commons.QuratorDocument;
import de.qurator.commons.conversion.QuratorSerialization;
import de.qurator.commons.conversion.nif.ITSRDF;
import de.qurator.commons.conversion.nif.TIME;
import de.unihd.dbs.heideltime.standalone.DocumentType;
import de.unihd.dbs.heideltime.standalone.HeidelTimeStandalone;
import de.unihd.dbs.heideltime.standalone.OutputType;
import de.unihd.dbs.heideltime.standalone.POSTagger;
import de.unihd.dbs.heideltime.standalone.exceptions.DocumentCreationTimeMissingException;
import de.unihd.dbs.uima.annotator.heideltime.resources.Language;
import de.unihd.dbs.uima.types.heideltime.Timex3Interval;

/**
 * @author Julian Moreno Schneider julian.moreno_schneider@dfki.de, Peter Bourgonje peter.bourgonje@dfki.de
 *
 * The whole documentation about openNLP examples can be found in https://opennlp.apache.org/documentation/1.6.0/manual/opennlp.html
 *
 */

@Component
public class EHeideltimeService {

	Logger logger = Logger.getLogger(EHeideltimeService.class);

	@Value("${heildeltime.config.file:config.props}")
	private String CONFIG_PROPS; 
	//	private static final String CONFIG_PROPS = "config.props"; 

	@Value("${heideltime.config.path:/opt/heideltime-kit/conf/config.props}")
	private String RESOURCE_NAME;
	//	private static final String RESOURCE_NAME = "/opt/heideltime-kit/conf/" + CONFIG_PROPS; 
	//private static final String RESOURCE_NAME = "C:\\Users\\pebo01\\Desktop\\heideltime-kit\\conf\\" + CONFIG_PROPS;

	private String configPath; 

	private HashMap<String, HeidelTimeStandalone> heidels; 

	private Set<Interval> intervals = new TreeSet<>(); 
	private Set<Date> dates = new TreeSet<>(); 
	private Date normDate; 

	public EHeideltimeService() {
	} 

	@PostConstruct
	public void initializeModels(){
		try{
			//		InputStream in = EHeideltimeService.class.getResourceAsStream(RESOURCE_NAME); 
			System.out.println(RESOURCE_NAME);
			InputStream in = new FileInputStream(RESOURCE_NAME);
			String tempDir = System.getProperty("java.io.tmpdir"); 
			configPath = Paths.get(tempDir, CONFIG_PROPS).toString(); 

			// File f = new File(configPath); 
			// if (!f.exists()) { 
			OutputStream out = new FileOutputStream(configPath); 
			try { 
				IOUtils.copy(in, out); 
			} finally { 
				in.close(); 
				out.close(); 
			}

			heidels = new HashMap<String, HeidelTimeStandalone>();
			heidels.put("en", new HeidelTimeStandalone(Language.ENGLISH, DocumentType.NEWS, 
					OutputType.TIMEML, configPath, POSTagger.TREETAGGER, 
					true));
			heidels.put("de", new HeidelTimeStandalone(Language.GERMAN, DocumentType.NEWS, 
					OutputType.TIMEML, configPath, POSTagger.TREETAGGER, 
					true));
			heidels.put("es", new HeidelTimeStandalone(Language.SPANISH, DocumentType.NEWS, 
					OutputType.TIMEML, configPath, POSTagger.TREETAGGER, 
					true));
			heidels.put("fr", new HeidelTimeStandalone(Language.FRENCH, DocumentType.NEWS, 
					OutputType.TIMEML, configPath, POSTagger.TREETAGGER, 
					true));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	//	public Model analyzeText(Model nifModel, String languageParam) throws DocumentCreationTimeMissingException, ParseException, Exception {
	//		try {
	//			String document = NIFReader.extractIsString(nifModel);
	//			Calendar cal = Calendar.getInstance(); 
	//			Date documentCreationTime = cal.getTime();
	//			String prefix = NIFReader.extractDocumentURI(nifModel);
	//			Model nifOutModel = doProcessNIF(nifModel, documentCreationTime, languageParam);
	//			if (nifOutModel!=null && dates.size() > 2) { 
	//				nifOutModel = doProcess(document, normDate,languageParam,prefix); 
	//			} 
	//			return nifOutModel;
	//    	} catch (Exception e2) {
	//        	logger.error(e2.getMessage());
	//    		throw e2;
	//    	}
	//	}

	//	private Model doProcessNIF(Model nifModel, Date documentCreationTime, String languageParam) throws DocumentCreationTimeMissingException, ParseException, Exception { 
	//		dates.clear(); 
	//		normDate = null; 
	//		intervals.clear(); 
	//
	//		boolean success = false; 
	//
	//		String text = NIFReader.extractIsString(nifModel);
	//		
	//		NIFTimeResultFormatter rf = new NIFTimeResultFormatter();
	//		String result = heidels.get(languageParam).process(text + ".", documentCreationTime, rf); 
	//
	//		Map<Integer, Timex3Interval> timex3Intervals = rf.getIntervals(); 
	//		Map<Integer, Timex3> timexes = rf.getTimexes(); 
	//
	//		if (!timex3Intervals.isEmpty()) { 
	//			for (Timex3Interval interval : timex3Intervals.values()) { 
	//				Timex3IntervalAsDateDecorator decorated = new Timex3IntervalAsDateDecorator( 
	//						interval); 
	//				intervals.add(new Interval(decorated.getEarliestBegin(), 
	//						decorated.getLatestEnd()));
	//				
	////				System.out.println("-----");
	////				System.out.println(interval.getTimexValueEB());
	////				System.out.println(interval.getTimexValueLE());
	////				System.out.println(interval.getBegin());
	////				System.out.println(interval.getEnd());
	////				System.out.println(interval.getBeginTimex());
	////				System.out.println(interval.getEndTimex());
	////				System.out.println(interval.getTimexValue());
	////				System.out.println(interval.getCoveredText());
	////				System.out.println("-----");
	//				NIFWriter.addTemporalEntityFromTimeML(nifModel, 
	//						interval.getBegin(), 
	//						interval.getEnd(),
	//						interval.getCoveredText(), 
	//						interval.getTimexValueEB()+"_"+interval.getTimexValueLE());
	//				
	//			} 
	//			success = true; 
	//		} 
	//
	////		if (!timexes.isEmpty()) { 
	////			for (Iterator<Timex3> iterator = timexes.values().iterator(); iterator 
	////					.hasNext();) { 
	////				Timex3 timex = iterator.next(); 
	////				Timex3AsDateDecorator decorated = new Timex3AsDateDecorator( 
	////						timex); 
	//////				dates.add(decorated.getValueAsDate()); 
	//////				if (!iterator.hasNext()) { 
	//////					normDate = decorated.getValueAsDate(); 
	//////				} 
	////
	////				normDate = timex.getTimexValue();
	////				//TODO Include all the timexes information into NIF.
	////
	////			} 
	////			success = true; 
	////		} 
	//		if(success){
	////			System.out.println(NIFReader.model2String(nifModel, RDFSerialization.TURTLE));
	//			return nifModel;
	//		}
	//		return null; 
	//	} 

	public QuratorDocument annotateTime(QuratorDocument doc, String languageParam) throws DocumentCreationTimeMissingException, Exception {
		try {
			Calendar cal = Calendar.getInstance(); 
			Date documentCreationTime = cal.getTime();
			System.out.println("PRE-DOPROCESS:" + QuratorSerialization.toRDF(doc, "TURTLE"));
			QuratorDocument auxDoc = doProcess(doc, documentCreationTime, languageParam, doc.getId()); 
			System.out.println("POST-DOPROCESS" + QuratorSerialization.toRDF(auxDoc, "TURTLE"));
			if (auxDoc!=null && dates.size() > 2) { 
				auxDoc = doProcess(doc, normDate,languageParam, doc.getId()); 
			} 
			return auxDoc;
		} catch (Exception e2) {
			logger.error(e2.getMessage());
			throw e2;
		}
	}

	public QuratorDocument annotateTimeInText(String document, String languageParam, String prefix) throws DocumentCreationTimeMissingException, ParseException, Exception  {
		Calendar cal = Calendar.getInstance(); 
		Date documentCreationTime = cal.getTime();
		QuratorDocument qd = new QuratorDocument(document);
		QuratorDocument auxDoc = doProcess(qd, documentCreationTime, languageParam, prefix); 
		if (auxDoc!=null && dates.size() > 2) { 
			auxDoc = doProcess(qd, normDate,languageParam,prefix); 
		} 
		return auxDoc;
	}

	/**
	 *  
	 * @param document 
	 * @return true if the given document has been successfully processed, i.e. 
	 *         a date interval or a single date has been found. 
	 * @throws DocumentCreationTimeMissingException 
	 * @throws ParseException 
	 */ 
	private QuratorDocument doProcess(QuratorDocument doc, Date documentCreationTime, String languageParam, String prefix) throws DocumentCreationTimeMissingException, ParseException, Exception  { 
		dates.clear(); 
		normDate = null; 
		intervals.clear(); 

		boolean success = false; 
		//		Model nifModel = NIFWriter.initializeOutputModel();
		////		NIFWriter.addInitialString(nifModel, document, "http://dkt.dfki.de/heideltime/101");
		//		NIFWriter.addInitialString(nifModel, document, prefix);		

		NIFTimeResultFormatter rf = new NIFTimeResultFormatter();

		String document = doc.getText();
		System.out.println(document);
		System.out.println(documentCreationTime);

		String result = heidels.get(languageParam).process(document + ".", documentCreationTime, rf); 
		Map<Integer, Timex3Interval> timex3Intervals = rf.getIntervals(); 
		//		Map<Integer, Timex3> timexes = rf.getTimexes(); 

		if (!timex3Intervals.isEmpty()) { 
			for (Timex3Interval interval : timex3Intervals.values()) { 
				Timex3IntervalAsDateDecorator decorated = new Timex3IntervalAsDateDecorator( 
						interval); 
				intervals.add(new Interval(decorated.getEarliestBegin(), 
						decorated.getLatestEnd()));

				//				System.out.println("-----");
				//				System.out.println(interval.getTimexValueEB());
				//				System.out.println(interval.getTimexValueLE());
				//				System.out.println(interval.getBegin());
				//				System.out.println(interval.getEnd());
				//				System.out.println(interval.getBeginTimex());
				//				System.out.println(interval.getEndTimex());
				//				System.out.println(interval.getTimexValue());
				//				System.out.println(interval.getCoveredText());
				//				System.out.println("-----");

				//Add annotation to DOC.
				//				String normalization = "";
				//				// convert normalization to xsd:dateTime notation
				//				String[] norm = normalization.split("_");
				//				System.out.println("D1: "+norm[0]);
				//				System.out.println("D2: "+stringDateXSDDateTimeFormatter(norm[0]));
				//				String intervalStart = stringDateXSDDateTimeFormatter(norm[0]);
				//				String intervalEnd = stringDateXSDDateTimeFormatter(norm[1]);
				////				String[] norm = normalization.split("_");
				////				String intervalStart = norm[0];
				////				String intervalEnd = norm[1];

				Map<String, String> properties = new HashMap<String, String>();
				properties.put(TIME.intervalStarts.getURI(), interval.getTimexValueEB());
				properties.put(TIME.intervalFinishes.getURI(), interval.getTimexValueLE());
				properties.put(ITSRDF.taClassRef.getURI(), TIME.temporalEntity.getURI());
				//				properties.put(, "option4");

				List<Label> labels =  new LinkedList<Label>();
				labels.add(new Label(properties));
				LabelPositionAnnotation lpa = new LabelPositionAnnotation(doc.getContext(), 
						interval.getBegin(), 
						interval.getEnd(),
						interval.getCoveredText(),
						labels);
				doc.addAnnotation(lpa);				
			} 
			success = true; 
		} 

		//		if (!timexes.isEmpty()) { 
		//			for (Iterator<Timex3> iterator = timexes.values().iterator(); iterator 
		//					.hasNext();) { 
		//				Timex3 timex = iterator.next(); 
		//				Timex3AsDateDecorator decorated = new Timex3AsDateDecorator( 
		//						timex); 
		////				dates.add(decorated.getValueAsDate()); 
		////				if (!iterator.hasNext()) { 
		////					normDate = decorated.getValueAsDate(); 
		////				} 
		//
		//				normDate = timex.getTimexValue();
		//				//TODO Include all the timexes information into NIF.
		//
		//			} 
		//			success = true; 
		//		} 

		//		/**
		//		 * TODO Include meanDate for the document.
		//		 */
		//		String meanDateRange = "";
		//		String[] norm2 = meanDateRange.split("_");
		//		String intervalStart2 = stringDateXSDDateTimeFormatter(norm2[0]);
		//    	String intervalEnd2 = stringDateXSDDateTimeFormatter(norm2[1]);
		//    	
		//		Map<String, String> properties2 = new HashMap<String, String>();
		//		properties2.put("qont:meanDateStart", intervalStart2);
		//		properties2.put("qont:meanDateEnd", intervalEnd2);
		//		List<Label> labels2 =  new LinkedList<Label>();
		//		labels2.add(new Label(properties2));
		//    	LabelAnnotation la = new LabelAnnotation(doc.getContext(), labels2);
		//    	doc.addDocumentAnnotation(la);

		if(success){
			//			System.out.println(NIFReader.model2String(nifModel, RDFSerialization.TURTLE));
			return doc;
		}
		return null; 
	} 

	private static String stringDateXSDDateTimeFormatter(String date){
		String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		String hour = date.substring(8,10);
		String minute = date.substring(10, 12);
		String second = date.substring(12, 14);
		return String.format("%s-%s-%sT%s:%s:%s", year, month, day, hour, minute, second);
	}

	//	public Set<Interval> getIntervals() { 
	//		return this.intervals; 
	//	} 
	//
	//	public Set<Date> getDates() { 
	//		return this.dates; 
	//	} 
	//
	//	public <T> T getLastElement(final Collection<T> c) { 
	//		final Iterator<T> itr = c.iterator(); 
	//		T lastElement = itr.next(); 
	//		while (itr.hasNext()) { 
	//			lastElement = itr.next(); 
	//		} 
	//		return lastElement; 
	//	} 

	//	/**
	//	 * Checks if two date objects are on the same day ignoring time. 
	//	 *  
	//	 * @param date1 
	//	 * @param date2 
	//	 * @return true if they represent the same day 
	//	 * @throws java.lang.IllegalArgumentException 
	//	 *             if either date is null 
	//	 */ 
	//	private boolean isSameDay(Date date1, Date date2) { 
	//		if (date1 == null || date2 == null) { 
	//			throw new IllegalArgumentException("The date must not be null"); 
	//		} 
	//		Calendar cal1 = Calendar.getInstance(); 
	//		cal1.setTime(date1); 
	//		Calendar cal2 = Calendar.getInstance(); 
	//		cal2.setTime(date2); 
	//		return isSameDay(cal1, cal2); 
	//	} 

	//	/**
	//	 * Checks if two calendar objects are on the same day ignoring time. 
	//	 *  
	//	 * @param cal1 
	//	 * @param cal2 
	//	 * @return true if they represent the same day 
	//	 * @throws java.lang.IllegalArgumentException 
	//	 *             if either calendar is null 
	//	 * @see org.apache.commons.lang.time.DateUtils 
	//	 */ 
	//	private boolean isSameDay(Calendar cal1, Calendar cal2) { 
	//		if (cal1 == null || cal2 == null) { 
	//			throw new IllegalArgumentException("The date must not be null"); 
	//		} 
	//		return (/*
	//		 * cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && 
	//		 */cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1 
	//		 .get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)); 
	//	} 

	/**
	 * A time interval represents a period of time between two instants. 
	 */ 
	public class Interval implements Comparable<Interval> { 
		private Date startTime; 
		private Date stopTime; 

		public Interval() { 
		} 

		public Interval(Date startTime, Date stopTime) { 
			setStartTime(startTime); 
			setStopTime(stopTime); 
		} 

		public Date getStartTime() { 
			return startTime; 
		} 

		public Date getStopTime() { 
			return stopTime; 
		} 

		public void setStartTime(Date startTime) { 
			this.startTime = startTime; 
		} 

		public void setStopTime(Date stopTime) { 
			this.stopTime = stopTime; 
		} 

		@Override 
		public int compareTo(Interval o) { 
			long thisTime = startTime.getTime() + stopTime.getTime(); 
			long anotherTime = o.startTime.getTime() + o.stopTime.getTime(); 
			return (thisTime < anotherTime ? -1 : (thisTime == anotherTime ? 0 
					: 1)); 
		} 

		@Override 
		public int hashCode() { 
			final int prime = 31; 
			int result = 1; 
			result = prime * result 
					+ ((startTime == null) ? 0 : startTime.hashCode()); 
			result = prime * result 
					+ ((stopTime == null) ? 0 : stopTime.hashCode()); 
			return result; 
		} 

		@Override 
		public boolean equals(Object obj) { 
			if (this == obj) 
				return true; 
			if (obj == null) 
				return false; 
			if (getClass() != obj.getClass()) 
				return false; 
			Interval other = (Interval) obj; 
			if (startTime == null) { 
				if (other.startTime != null) 
					return false; 
			} else if (!startTime.equals(other.startTime)) 
				return false; 
			if (stopTime == null) { 
				if (other.stopTime != null) 
					return false; 
			} else if (!stopTime.equals(other.stopTime)) 
				return false; 
			return true; 
		} 

	} 

	//	public static void main(String[] args) throws DocumentCreationTimeMissingException, ParseException, IOException, Exception { 
	//		String[] documents = new String[] { 
	//				"Du 21 au 30 janvier 2015", 
	//				// "Tous les samedis et vendredis de 20h à 22h00.", Not yet 
	//				// managed 
	//				"mer 28 jan 15 à 20:00.", 
	//				"lun 27 avr 15, 20:00", 
	//				"sam 3 jan", 
	//				"Du 11 novembre 2014 au 15 janvier 2015", 
	//				"Le 16 janvier 2015", 
	//				"mercredi 1er samedi 4 et dimanche 5 avril à 10h30 et 16h45", 
	//				"Le mercredi 4 et le samedi 7 et le dimanche 8 mars" /*
	//				 * à 10h30 
	//				 * et 
	//				 * 16h45" 
	//				 */, 
	//				 "mercredi 4 et samedi 7 et dimanche 8 mars à 10h30 et 16h45", 
	//				 "Le mercredi 4 et le dimanche 8 mars", 
	//				 "Le mercredi 4", 
	//				 "21.03", 
	//				 "le lundi, mardi et jeudi", 
	//				 "mercredi 1er avril à 10h30 et 16h45", 
	//				 "lundi 20, mardi 21 mercredi 22 jeudi 23 vendredi 24 samedi 25 et dimanche 26 avril à 10h30 et 16h45", 
	//				 "9-10-11 AVRIL 15", "27.03.15", "DU 05/05/2015 AU 20/12/2015", 
	//				 "le 8/11/2015 à 19:30", "20 nov. à 20h00", 
	//
	//				 // "Tous les jours du lundi 13 au dimanche 19 avril", 
	//		}; 
	//
	//		Map<String, EHeideltimeService> results = new HashMap<>(); 
	//
	//		for (String document : documents) { 
	//			EHeideltimeService dateTimeProcessor = new EHeideltimeService(); 
	//			dateTimeProcessor.annotateTimeInText(document.toLowerCase(), "fr", null); // TODO: thinking 
	//			// about the case 
	//			results.put(document, dateTimeProcessor); 
	//		} 
	//
	////		// Display results 
	////		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, 
	////				DateFormat.SHORT, Locale.FRENCH); 
	////		Set<Entry<String, EHeideltimeService>> entries = results.entrySet(); 
	////
	////		for (Entry<String, EHeideltimeService> entry : entries) { 
	////			System.out.println("--------------------------------------"); 
	////			System.out.println("Source = \"" + entry.getKey() + "\""); 
	////			EHeideltimeService dtp = entry.getValue(); 
	////
	////			Set<Interval> intervals = dtp.getIntervals(); 
	////			if (!intervals.isEmpty()) { 
	////				System.out.println("-- Intervals found --"); 
	////				for (Interval interval : intervals) { 
	////					System.out.println("Du " 
	////							+ df.format(interval.getStartTime()) + " au " 
	////							+ df.format(interval.getStopTime())); 
	////				} 
	////			} 
	////
	////			Set<Date> dates = dtp.getDates(); 
	////			if (!dates.isEmpty()) { 
	////				System.out.println("-- Dates found --"); 
	////				for (Date date : dates) { 
	////					System.out.println(df.format(date)); 
	////				} 
	////			} 
	////
	////		} 
	//	} 

	public static void main(String[] args) throws Exception {
		//		//		InputStream in = EHeideltimeService.class.getResourceAsStream(RESOURCE_NAME); 
		//		String configPath = "/Users/julianmorenoschneider/Documents/DFKI/code/heidel/heideltime-kit/conf/config.props";
		//		
		//		HeidelTimeStandalone heidel = new HeidelTimeStandalone(Language.ENGLISH, DocumentType.NEWS, 
		//				OutputType.TIMEML, configPath, POSTagger.TREETAGGER, 
		//				true);
		//		
		//		NIFTimeResultFormatter rf = new NIFTimeResultFormatter();
		//
		//		String text = "Welcome to Berlin in 2016";
		//
		//		Date documentCreationTime = new Date();
		//		System.out.println(text);
		//		System.out.println(documentCreationTime);
		//		String result = heidel.process(text + ".", documentCreationTime, rf);//new TimeMLResultFormatter()); 
		//
		//		System.out.println("RESULT: "+result);

		String user = "Lynx";
		String password = "l!n10_pr0j3ct";

		HttpResponse<String> response = Unirest.post("https://services.tilde.com/Translation/lynx/?sourceLang=en&targetLang=de")
				.header("Accept", "application/turtle")
				.header("Content-Type", "text/plain")
				.queryString("sourceLang", "en")
				.queryString("targetLang", "de")
				.basicAuth(user, password)
				.body("There is some text here and I want to translate it. That is interesting for me.")
				.asString();


		System.out.println("RESULT: " + response.getBody());
	}

	public JSONArray annotateTimeInELGJson(String inputString, String language, Date documentCreationTime) throws Exception {
		dates.clear(); 
		normDate = null; 
		intervals.clear(); 

		JSONArray array = new JSONArray();
		boolean success = false; 
		NIFTimeResultFormatter rf = new NIFTimeResultFormatter();
		String document = new JSONObject(inputString).getString("content");
		/**
		 * TODO Extract creation date from input JSON.
		 */
		System.out.println(document);
		System.out.println(documentCreationTime);

		String result = heidels.get(language).process(document + ".", documentCreationTime, rf);
		Map<Integer, Timex3Interval> timex3Intervals = rf.getIntervals(); 

		if (!timex3Intervals.isEmpty()) { 
			for (Timex3Interval interval : timex3Intervals.values()) { 
				Timex3IntervalAsDateDecorator decorated = new Timex3IntervalAsDateDecorator( 
						interval); 
				intervals.add(new Interval(decorated.getEarliestBegin(), 
						decorated.getLatestEnd()));

				JSONObject features = new JSONObject();
				features.put("nif:anchorOf", interval.getCoveredText());
				features.put(TIME.intervalStarts.getURI(), interval.getTimexValueEB());
				features.put(TIME.intervalFinishes.getURI(), interval.getTimexValueLE());
				features.put(ITSRDF.taClassRef.getURI(), TIME.temporalEntity.getURI());
				JSONObject js = new JSONObject();
				js.put("start", interval.getBegin());
				js.put("end", interval.getEnd());
				js.put("features", features);
				array.put(js);
			} 
			success = true; 
		} 
		/**
		 * TODO Include meanDate for the document.
		 */
//		String meanDateRange = "";
//		String[] norm2 = meanDateRange.split("_");
//		String intervalStart2 = stringDateXSDDateTimeFormatter(norm2[0]);
//		String intervalEnd2 = stringDateXSDDateTimeFormatter(norm2[1]);
//
//		Map<String, String> properties2 = new HashMap<String, String>();
//		properties2.put("qont:meanDateStart", intervalStart2);
//		properties2.put("qont:meanDateEnd", intervalEnd2);
//		List<Label> labels2 =  new LinkedList<Label>();
//		labels2.add(new Label(properties2));
//		LabelAnnotation la = new LabelAnnotation(doc.getContext(), labels2);
//		doc.addDocumentAnnotation(la);

		if(success){
			return array;
		}
		return null; 
	}
}
