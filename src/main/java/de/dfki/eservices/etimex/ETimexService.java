package de.dfki.eservices.etimex;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import de.dfki.eservices.etimex.eheideltime.EHeideltimeService;
import de.dfki.eservices.etimex.persistence.Analysis;
import de.dfki.eservices.etimex.persistence.AnalysisRepository;
import de.qurator.commons.QuratorDocument;
import de.qurator.commons.conversion.QuratorDeserialization;
import de.qurator.commons.conversion.QuratorSerialization;

/**
 * @author Julian Moreno Schneider julian.moreno_schneider@dfki.de, Peter Bourgonje peter.bourgonje@dfki.de
 *
 * The whole documentation about openNLP examples can be found in https://opennlp.apache.org/documentation/1.6.0/manual/opennlp.html
 *
 */

@Component
public class ETimexService {

	Logger logger = Logger.getLogger(ETimexService.class);
	
//	@Autowired
//	EOpenNLPService opennlpService;
	
	@Autowired
	EHeideltimeService heideltimeService;
	
	public ETimexService(){
	}

	@PostConstruct
	public void initializeModels(){
	}
	
	@Autowired
	AnalysisRepository analysisRepository;
	
	HashMap<String, Analysis> analysiss = new HashMap<String, Analysis>();
	
	public String analyzeSynchronous(String textForProcessing, String informat, String outformat, String languageParam, boolean isContent, String outputCallback) throws Exception, Exception,IOException, Exception {
		try {
			
			System.out.println("INPUT TEXT: " + textForProcessing);
			QuratorDocument doc = null;
    		
    		if(isContent) {
    			if(informat.equalsIgnoreCase("text/plain")) {
        			doc = new QuratorDocument(textForProcessing);
    			}
    			else if(informat.equalsIgnoreCase("text/turtle")) {
        			doc = QuratorDeserialization.fromRDF(textForProcessing, informat);
    			}
    			else if(informat.equalsIgnoreCase("application/json+ld")) {
        			doc = QuratorDeserialization.fromJSONLD(textForProcessing);
    			}
    			else {
    				throw new Exception("ERROR: informat/contentType not supported: "+informat);
    			}
            }
    		else {
    			//TODO Get information from the triple store.
//    			nifModel = NIFAdquirer.obtainNIFDocumentFromTripleStore(textForProcessing, tripleStoreURL, tripleStoreName);
//    			nifModel = NIFAdquirer.obtainNIFDocumentFromFileStore(textForProcessing, informat);
    		}
    		if(doc==null) {
    			throw new Exception("QuratorDocument is NULL before processing NER!!");
    		}
//    		System.out.println(doc.toRDF("TURTLE"));
       		doc = heideltimeService.annotateTime(doc, languageParam);
       		if(outformat.equalsIgnoreCase("text/turtle")) {
    			return QuratorSerialization.toRDF(doc, outformat);
    		}
    		if(outformat.equalsIgnoreCase("application/json+ld")) {
    			return QuratorSerialization.toJSON(doc);
    		}
    		else {
    			throw new Exception("OutputFormat not supported.");
    		}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

	}

	
	public String analyzeAsynchronous(String textForProcessing, String informat, String outformat, String languageParam, boolean isContent, String outputCallback) throws Exception, Exception,IOException, Exception {
		try {
			String analysisId = generateAnalysisId();
			Analysis a = new Analysis();
			a.setAnalysisId(analysisId);
			a.setInput(textForProcessing);
			a.setInputFormat(informat);
			a.setOutputFormat(outformat);
			analysisRepository.save(a);
			analysiss.put(analysisId, a);
			CompletableFuture<Void> contentFuture = CompletableFuture.supplyAsync(() -> {
				String s = "";
				try {
//					System.out.println(textForProcessing);
					QuratorDocument doc = null;
		    		
		    		if(isContent) {
		    			if(informat.equalsIgnoreCase("text/plain")) {
		        			doc = new QuratorDocument(textForProcessing);
		    			}
		    			else if(informat.equalsIgnoreCase("text/turtle")) {
		        			doc = QuratorDeserialization.fromRDF(textForProcessing, informat);
		    			}
		    			else if(informat.equalsIgnoreCase("application/json+ld")) {
		        			doc = QuratorDeserialization.fromJSONLD(textForProcessing);
		    			}
		    			else {
		    				throw new Exception("ERROR: informat/contentType not supported: "+informat);
		    			}
		            }
		    		else {
		    			//TODO Get information from the triple store.
//		    			nifModel = NIFAdquirer.obtainNIFDocumentFromTripleStore(textForProcessing, tripleStoreURL, tripleStoreName);
//		    			nifModel = NIFAdquirer.obtainNIFDocumentFromFileStore(textForProcessing, informat);
		    		}
		    		if(doc==null) {
		    			throw new Exception("QuratorDocument is NULL before processing NER!!");
		    		}
//		    		System.out.println(doc.toRDF("TURTLE"));
		       		doc = heideltimeService.annotateTime(doc, languageParam);
		    		if(outformat.equalsIgnoreCase("text/turtle")) {
		    			return QuratorSerialization.toRDF(doc, outformat);
		    		}
		    		if(outformat.equalsIgnoreCase("application/json+ld")) {
		    			return QuratorSerialization.toJSON(doc);
		    		}
		    		else {
		    			throw new Exception("OutputFormat not supported.");
		    		}
				} catch (Exception e) {
					e.printStackTrace();
					s = "EXCEPTION";
				}
				return s;
			}).thenAccept(t -> {
				processOutput(analysisId, textForProcessing, informat, t, outformat, outputCallback);
			});
			return analysisId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public void processOutput(String analysisId, String input, String informat, String output, String outformat,
			String outputCallback) {
		try {
			Analysis a2 = analysiss.get(analysisId);
			a2.setAnalysisId(analysisId);
			a2.setInput(input);
			a2.setInputFormat(informat);
			a2.setOutput(output);
			a2.setOutputFormat(outformat);
			a2.setFinished();
//			System.out.println("saving new analysis: "+a2.getAnalysisId()+" with status: "+a2.getStatus2());
//			analysisRepository.save(a2);
			analysiss.put(analysisId,a2);
//			System.out.println("Status2 after updating:"+a3.getStatus2());
			if(outputCallback!=null && !outputCallback.equalsIgnoreCase("")) {
				//							System.out.println("OUTPUTCALLBACK: "+outputCallback);
				//							System.out.println("OUTPUTCALLBACK BODY: "+s);
				HttpResponse<String> callbackResponse = Unirest.post(outputCallback).body(output).asString();
				if(callbackResponse.getStatus()!=200) {
					throw new Exception("Error reporting outputCallback from Microservice Geolocation for Analysis [" + analysisId + "] with ERROR: " + callbackResponse.getStatus() + " ["+callbackResponse.getBody()+"]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String generateAnalysisId() {
		Date d = new Date();
		return d.getTime()+"";
	}
	
	public String checkStatus(String analysisId) throws Exception {
		Analysis a = analysisRepository.findOneByAnalysisId(analysisId);
		if(a==null) {
			throw new Exception("The provided 'analysisId' does not match any existing Analysis.");
		}
		return a.getStatus().toString();
	}

	public String getOutput(String analysisId, String contentTypeHeader) throws Exception {
		boolean done = false;
		Analysis a = null;
//		List<Analysis> analyses = analysisRepository.findAll();
//		System.out.println("AVAILABLE ANALYSIS:");
//		for (Analysis analysis : analyses) {
//			System.out.println(analysis.getAnalysisId());
//		}
		while(!done) {
//			a = analysisDAO.findOneByAnalysisId(analysisId);
			a = analysiss.get(analysisId);
			if(a==null) {
//				System.out.println("Sleep because is NULL...");
				Thread.sleep(1000);
//				throw new Exception("The provided 'analysisId' does not match any existing Analysis.");
			}
			else {
				if(a.getStatus2().equalsIgnoreCase("FINISHED")) {
					done=true;
				}
				else {
//					System.out.println(a.getStatus2());
//					System.out.println("Sleep because is NOT FINISHED...");
					Thread.sleep(1000);
				}
			}
		}
		String output = a.getOutput();
		if(a.getOutputFormat().equalsIgnoreCase(contentTypeHeader)) {
			return output;
		}
		else {
//			System.out.println(contentTypeHeader);
//			System.out.println(a.getOutputFormat());
//			System.out.println(output);
			QuratorDocument qd = new QuratorDocument();
			QuratorDocument qd2 = QuratorDeserialization.fromRDF(output, a.getOutputFormat());
			return QuratorSerialization.toRDF(qd2, "turtle");
			
		}
	}

	public String analyzeELG(String textForProcessing, String contentTypeHeader, String acceptHeader, String language, Date creationDate) throws Exception {
		System.out.println("INPUT TEXT: " + textForProcessing);
		JSONArray annotations = heideltimeService.annotateTimeInELGJson(textForProcessing, language, creationDate);
		String jsonString = "{\n" + 
        		"                \"response\": {\n" + 
        		"                    \"type\": \"annotations\",\n" + 
        		"                    \"annotations\": {\n" + 
        		"                        \"Temporal Expression\": "+annotations+"\n" + 
        		"                    }\n" + 
        		"                }\n" + 
        		"            }";
        JSONObject json = new JSONObject(jsonString);
		return json.toString();
	}
	
//	public QuratorDocument analyze(QuratorDocument doc, String languageParam, String models,  String inFormat, String mode) 
//			throws Exception, Exception,IOException, Exception {
//        try {
//       		doc = heideltimeService.annotateTime(doc, languageParam);
//       		return doc;
//        } catch (Exception e) {
//        	logger.error(e.getMessage());
//            throw e;
//    	}        
//	}
}
