package de.dfki.eservices.etimex.persistence;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Complex database functionality for workflowsDefinition
 * 
 * @author Julian Moreno Schneider jumo04@dfki.de
 */
@Component
public class AnalysisDAO {

	@Autowired
	AnalysisRepository analysisRepository;

	@Autowired
	EntityManager entityManager;

	public void save(Analysis analysis) {
		analysisRepository.save(analysis);
		entityManager.persist(analysis);
	}
	
	public void update(Analysis analysis) {
//		analysisRepository.save(analysis);
		entityManager.refresh(analysis);
	}

	@Transactional
	public Analysis findOneByAnalysisId(String analysisId) throws Exception {
		Analysis ana = analysisRepository.findOneByAnalysisId(analysisId);
		if(ana==null){
			String msg = String.format("The analysis \"%s\" does not exist.",analysisId);
        	throw new Exception(msg);
		}
		return ana;
	}

	@Transactional
	public void deleteByAnalysisId(String analysisId) {
		Analysis ana = analysisRepository.findOneByAnalysisId(analysisId);
		entityManager.remove(ana);
	}
}
