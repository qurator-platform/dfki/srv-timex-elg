package de.dfki.eservices.etimex.persistence;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Julian Moreno Schneider jumo04@dfki.de
 */
@Repository
public interface AnalysisRepository extends CrudRepository<Analysis, Long> {

	public List<Analysis> findAll();

	@Transactional
	public Analysis findOneByAnalysisId(String analysisId);

	@Transactional
	public void deleteByAnalysisId(String analysisId);
}
