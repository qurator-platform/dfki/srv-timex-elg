package de.dfki.eservices.etimex.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Analysis {

	public enum Status {
		PAUSED, RUNNING, FINISHED, ERROR
	}

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	String analysisId;

	//	@JoinColumn(name = "indexId")
	@Column(columnDefinition="LONGVARCHAR")
	String input;
	String inputURL;
	String inputFormat;
	
	@Column(columnDefinition="LONGVARCHAR")
	String output;
	String outputFormat;

	@Enumerated(EnumType.STRING)
//    @Column(length = 8)
	Status status;

	String status2;
	
	public Analysis() {
		status = Status.RUNNING;
		status2 = "RUNNING";
	}
	
	public void setFinished() {
//		System.out.println("CHANGING status from "+analysisId+" to FINISHED.");
		status = Status.FINISHED;
		status2 = "FINISHED";
		
	}
	
	public void setPause() {
		status = Status.PAUSED;
		status2 = "PAUSED";
	}
	
	public String getAnalysisId() {
		return analysisId;
	}

	public void setAnalysisId(String analysisId) {
		this.analysisId = analysisId;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getInputURL() {
		return inputURL;
	}

	public void setInputURL(String inputURL) {
		this.inputURL = inputURL;
	}

	public String getInputFormat() {
		return inputFormat;
	}

	public void setInputFormat(String inputFormat) {
		this.inputFormat = inputFormat;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public String getStatus2() {
		return status2;
	}

	public void setStatus2(String status2) {
		this.status2 = status2;
	}
	
	
//	@Column(columnDefinition="LONGVARCHAR")
//	String workflowDescription;

//	@OneToMany(cascade=CascadeType.PERSIST)
//	@ElementCollection
//	List<WorkflowComponent> components;
//	@ElementCollection
//	@Lob
//	private List<String> componentsDefinitions;

//	Date creationTime;

//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	List<WorkflowComponent> components;
	
	
}
