package de.dfki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author jumo04 - Julian Moreno Schneider
 * Class executing the spring boot application
 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
//@EnableConfigurationProperties(EConversionRestAPI.class)
//@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,WebMvcAutoConfiguration.class })
@ComponentScan(basePackages = {"de.dfki"})

public class ETimexApplication {

	/**
	 * Main class starting the Spring Application
	 * @param args
	 */
	public static void main(String[] args) {
        SpringApplication.run(ETimexApplication.class, args);
    }

}
